#ifndef HD44780_H_
#define HD44780_H_

#include "stm32f0xx_hal.h"

/* 
 * Obsługa wyświetlacza HD44780
 * 16x2 po 8/4 bity na dane
 * Opracowane ze źródła:
 *      - http://www.embeddeddev.pl/obsluga-lcd-hd44780-komunikacja-jednokierunkowa/
 */


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define USE_COUNTER_DELAY
#ifdef USE_COUNTER_DELAY
#include "timers.h"
#define delay(t)                   (counter_delay(t))
#else
#define delay(t)                   (HAL_Delay(t))

#endif

#ifdef HD44780_8BIT_MODE
#define HD44780_USED_PINS		   (10)
#else
#define HD44780_USED_PINS		   (6)
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define CMD_CLEAR                  (0x01)   // <-- Czyszczenie wyświetlacza
#define CMD_HOME                   (0x02)   // <-- Powrót kursora 
#define CMD_ENTRY_MODE             (0x04)   // <-- Tryb wprowadzania tekstu
#define ENTRY_SHIFT_DISPLAY        (0x00)   // <-- 
#define ENTRY_SHIFT_CURSOR         (0x01)   // <-- 
#define ENTRY_LTR                  (0x00)   // <----- kierunek od lewej do prawej (*)
#define ENTRY_RTL                  (0x02)   // <----- Kierunek od prawej do lewej
#define CMD_CONTROLL               (0x08)   // <-- Sterowanie kilkoma bajerami
#define CONTROLL_ON                (0x04)   // <----- Włączenie wyświetlacza
#define CONTROLL_CURSOR            (0x02)   // <----- Włączenie kursora
#define CONTROLL_C_ON              (0x01)   // <-------- Efekt migającego kursora
#define CONTROLL_C_OFF             (0x00)   // <-------- Wyłącza powyższy efekt
#define CMD_SHIFT                  (0x10)   // <-- Przenoszenie kursora ("without changing DDRAM content")
#define SHIFT_CURSOR               (0x00)
#define SHIFT_DISPLAY              (0x08)
#define SHIFT_RIGHT                (0x04)
#define SHIFT_LEFT                 (0x00)
#define CMD_FUNCTION               (0x20)   // <-- Ustawienia
#define FUNCTION_8BIT              (0x10)   // <----- Tryb 8 bitów dla danych
#define FUNCTION_4BIT              (0x00)   // <----- Tryb 4 bitów dla danych
#define FUNCTION_2LINES            (0x08)   // <----- Tryb 2 linii
#define FUNCTION_FONT_5X10         (0x04)   // <----- Tryb czcionki
#define FUNCTION_FONT_5X7          (0x00)   // <----- Kolejny tryb czcionki

#define SET_CGRAM                  (0x40)   // <-- W CGRAM są znaki użytkownika
#define SET_DDRAM                  (0x80)   // <-- Dla określania pól wyświetlacza

#define LINE_1                     (0x00)   // <-- Pierwdza linia
#define LINE_2                     (0x40)   // <-- Druga linia

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define HD44780_SET_E              (HAL_GPIO_WritePin(LcdPorts[port_e] , LcdPins[pin_e] , GPIO_PIN_SET))
#define HD44780_RESET_E            (HAL_GPIO_WritePin(LcdPorts[port_e] , LcdPins[pin_e] , GPIO_PIN_RESET))
#define HD44780_SET_RS             (HAL_GPIO_WritePin(LcdPorts[port_rs] , LcdPins[pin_rs] , GPIO_PIN_SET))
#define HD44780_RESET_RS           (HAL_GPIO_WritePin(LcdPorts[port_rs] , LcdPins[pin_rs] , GPIO_PIN_RESET))
#define HD44780_CLEAR              (HD44780_SendCmd(CMD_CLEAR))

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

typedef enum {
	port_e = 0 , port_rs,

#ifdef HD44780_8BIT_MODE
	port_d0 , port_d1 , port_d2 , port_d3 ,
#endif

	port_d4 , port_d5 , port_d6 , port_d7
} HD44780_ports;

typedef enum {
	pin_e = 0 , pin_rs ,

#ifdef HD44780_8BIT_MODE
	pin_d0 , pin_d1 , pin_d2 , pin_d3 ,
#endif

	pin_d4 , pin_d5 , pin_d6 , pin_d7
} HD44780_pins;

GPIO_TypeDef* LcdPorts[HD44780_USED_PINS];
uint16_t LcdPins[HD44780_USED_PINS];
uint8_t cursor_x;
uint8_t cursor_y;
uint8_t lock_update;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void HD44780_Initialize     ();
void HD44780_SendHalf       (uint8_t data);
void HD44780_Send           (uint8_t data);
void HD44780_SendCmd        (uint8_t cmd);
void HD44780_SetCursor      (uint8_t x , uint8_t y);
void HD44780_SendChar       (uint8_t chr);
void HD44780_SendString     (uint8_t* chr , uint8_t len);

#endif
