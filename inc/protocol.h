#ifndef INC_PROTOCOL_H_
#define INC_PROTOCOL_H_

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"
#include "ringbuffer.h"
#include "hd44780.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define DATA_SIZE						9
#define SIZE_UNSET						100

#define FRAME_START						'<'
#define FRAME_STOP						'>'
#define FRAME_ESCAPE					'$'

#define CMD_NULL						0x00
#define CMD_STOP						0x01
#define CMD_START						0x02
#define CMD_SET							0x03
#define CMD_DIRECTION					0x04
#define CMD_BUZZER						0x05

#define STATE_NONE						0
#define STATE_PROCESSING				1
#define STATE_PROCESSED					2
#define STATE_ERROR						3

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define IS_NUMBER(x)					((x >= '0' && x <= '9') ? (1) : (0))
#define DIGIT_TO_NUMBER(x)				(x - 48)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Zmienna przechowująca rozmiar danych (odczytywana jako liczba)
uint8_t proto_size;

// Flaga znaku ucieczki (jeżeli inna niż 0)
uint8_t proto_escape;

// Znaki określające komendę
uint8_t proto_command[3];

// Znaki wprowadzonych danych
uint8_t proto_data[DATA_SIZE];

// Globalny iterator odebranych znakow
uint8_t proto_iter;

// Stan przetwarzania ramki (stan błędu blokuje przetwarzanie do czasu otrzymania znaku FRAME_START)
uint8_t proto_state;

// Poszczególne etapy przetwarzania ramki (włączane są kolejne bity)
uint8_t proto_progress;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 * Opis bitów dla zmiennej "proto_progress" progress:
 * 0 - start
 * 1 - cmd1
 * 2 - cmd2
 * 3 - cmd3
 * 4 - size
 * 5 - data
 * 6 - [unused]
 * 7 - stop
 * */

#define SET_PROGRESS_START()			(proto_progress &= 0x00); \
										(proto_progress |= 0x01)
#define SET_PROGRESS_CMD1()				(proto_progress |= 0x02)
#define SET_PROGRESS_CMD2()				(proto_progress |= 0x04)
#define SET_PROGRESS_CMD3()				(proto_progress |= 0x08)
#define SET_PROGRESS_SIZE()				(proto_progress |= 0x10)
#define SET_PROGRESS_DATA()				(proto_progress |= 0x20)
#define SET_PROGRESS_STOP()				(proto_progress |= 0x80)

#define GET_PROGRESS_START()			((proto_progress & 0x01) > 0 ? 1 : 0)
#define GET_PROGRESS_CMD1()				((proto_progress & 0x02) > 0 ? 1 : 0)
#define GET_PROGRESS_CMD2()				((proto_progress & 0x04) > 0 ? 1 : 0)
#define GET_PROGRESS_CMD3()				((proto_progress & 0x08) > 0 ? 1 : 0)
#define GET_PROGRESS_SIZE()				((proto_progress & 0x10) > 0 ? 1 : 0)
#define GET_PROGRESS_DATA()				((proto_progress & 0x20) > 0 ? 1 : 0)
#define GET_PROGRESS_STOP()				((proto_progress & 0x80) > 0 ? 1 : 0)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void protocol_process_data				(uint8_t byte);
void protocol_process_cmd				(uint8_t* cmd , uint8_t* data);
void protocol_clear						();
void protocol_write_error				(uint8_t* err);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif
