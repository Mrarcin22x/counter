#ifndef TIMERS_H_
#define TIMERS_H_

/* 
 * Wykonał: Marcin Spychalski (2020)
 * Funkcje obsługujące timer-y
 */

#include "main.h"
#include "hd44780.h"
#include <stdio.h>
#include <string.h>

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define BASE_TIM                   (htim2)

#define COUNT_UP                   (0)
#define COUNT_DOWN                 (1)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extern TIM_HandleTypeDef BASE_TIM;
extern uint8_t lock_update;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define COUNTER_ST_NULL				(0)
#define COUNTER_ST_STOP				(1)
#define COUNTER_ST_COUNTING			(2)
#define COUNTER_ST_ZERO				(3)

#define COUNTER_DIR_UP				(0)
#define COUNTER_DIR_DOWN			(1)

volatile uint8_t Cntr_state;
volatile uint8_t Cntr_min;
volatile uint8_t Cntr_sec;
volatile uint16_t Cntr_mili;
uint8_t Cntr_limit_min;
uint8_t Cntr_limit_sec;
uint8_t Cntr_direction;
uint8_t Cntr_beep;

volatile uint16_t delay_var;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void counter_delay          (uint16_t ms);
void counter_init_program	(uint8_t min , uint8_t sec , uint8_t dir , uint8_t beep);
void counter_set_state		(uint8_t state);
void counter_set_dir		(uint8_t state);
void counter_update_lcd		(uint8_t two_lines);

#endif
