# COUNTER
Program używa licznika TIM2 do odliczania czasu. To wszystko...\
\
Kod udostępniam z nadzieją, że dodasz coś od siebie i pozmieniasz.\
**NIE ZGADZAM SIĘ NA JAWNE UŻYCIE TEGO W TWOIM PROJEKCIE !!!**

## Peryferia
*  LCD (na sterowniku HD44780 16x2)
*  Buzzer

## Pliki
Poszczególne funkcje projektu zostały podzielone na dodatkowe pliki (.c + .h):
*  **hd44780** - obsługa wyświetlacza LCD (na podstawie poradników z internetu),
*  **ringbuffer** - realizacja bufora kołowego **(!!)**,
*  **protocol** - realizacja protolołu odbierającego i przetwarzającego ramki **(!!)**,
*  **flash** - amatorka, nie polecam używać w taki sposób tego używać,
*  **timers** - licznik, callback dla TIM2 i realizacja funkcji odliczania,
Zdaje się, że bufor i protokół to świętość, którą trzeba zrobić po prostu dobrze!,

## Bufor kołowy
Tego tłumaczyć nie będę. Należy to po prostu mieć i musi działać...

## Protokół
Z czego składa się protokół? To zależy co ma realizować. W projekcie powinien służyć min. do odpytywania urządzeć (np. odpytujesz czujnik temperatury jaka jest temperatura ;) ).
Każdy protokół składa się ze znaku początku ramki i końca (w moim projekcie są to kolejno znaki '**<**' i '**>**'). Wygląda to mniej więcej tak:
1.  **Znak początku ('<') - 1 znak** - oznacza, że od tego miejsca w odbieranym tekście nastąpi sprawdzanie czy odbierana jest ramka,
2.  **Komenda (wszystko z tablicy ASCII większe niż 31) - 3 znaki** - identyfikuje komendę, a jej zastosowanie to wybór programisty
3.  **Rozmiar danych (wartość '0' - '8') - 1 znak -** określa rozmiar kolejnego elementu ramki
4.  **Dane (wszystko z tablicy ASCII większe niż 31) - 0-8 znaków** - określa dowolne dane, które zostaną przekazane komendzie
5.  **Znak końca ('>') - 1 znak** - określa, że kończy się ramka.

### Poprawność ramki
Należy pamiętać, że ramka jest rmką jeżeli pasuje do opisu. Jeżeli coś się nie zgadza (np. jeżeli odbieram "<aaa2x>" to nie będzie to ramka -> rozmiar danych podano jako 2 a odebrano tylko jeden znak 'x') należy to traktować jak śmieci. Odebrane dane mogą być przekłamywane dlatego sprawdzanie poprawności jest takie ważne.

### Znak ucieczki
Komenda i dane umożliwiają odebranie prawie każdego znaku ASCII (również tych określanych jako początek ramki). Jednak nie można ich używać do niczego innego jak granic ramki! należy zakodować znaki wybierając znak ucieczki. Jest to znak, którego wystąpienie oznacza, że zostanie odebrany zakodowany (w 2 znaki) znak. U mnie jest to:
*  **$(** - znak '<'
*  **$)** - znak '>'
*  **$S** - znak '$'

> Znak specjalny nie może jawnie występować w odebranych danych. Dla protokołu znak zakodowany to 1 znak (elementr 'rozmiar' ramki). 
