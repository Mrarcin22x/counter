#include "ringbuffer.h"
#include "main.h"
#include "hd44780.h"
#include <stdio.h>
#include <string.h>

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

uint8_t transmit = 0;

/** *************************************************************************************************************** **/

uint8_t ringbuffer_write(uint8_t* data) {

	uint16_t idx = BuffTx_empty;
	__disable_irq();

	// Wpisywanie nowych danych do bufora
	for (uint16_t i = 0 ; i < strlen( (char*) data ) ; i++) {
		// Dopisywanie danych
		BuffTx_content[ idx ] = data[i];

		// Powrót na początek bufora
		if (++idx >= BUFF_TX_SIZE) idx = 0;
	}

	if (BuffTx_empty == BuffTx_bussy && transmit == 0) {
		transmit = 1;
		BuffTx_empty = idx;
		idx = BuffTx_bussy;

		// Transmisja znaku
		HAL_UART_Transmit_IT(&BASE_UART , &BuffTx_content[ idx ] , 1);

		// Powrót na początek bufora
		if (++idx >= BUFF_TX_SIZE) idx = 0;

		BuffTx_bussy = idx;
	} else BuffTx_empty = idx;
	__enable_irq();

	return 0;
}

/** *************************************************************************************************************** **/

int16_t ringbuffer_read() {

	int16_t result = -1;

	if (BuffRx_empty != BuffRx_bussy) {

		// Pobieranie znaku
		result = (int16_t) BuffRx_content[ BuffRx_bussy ];

		// Powrót na początek bufora
		if (++BuffRx_bussy >= BUFF_RX_SIZE) BuffRx_bussy = 0;
	}

	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	if (huart == &BASE_UART) {

		// Powrót na początek bufora
		if (++BuffRx_empty >= BUFF_RX_SIZE) BuffRx_empty = 0;

		// Odbieranie danych
		HAL_UART_Receive_IT(&BASE_UART , &BuffRx_content[ BuffRx_empty ] , 1);

	}
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart) {
	if (huart == &BASE_UART) {
		if (BuffTx_empty != BuffTx_bussy) {

			// Wysyłanie znaku
			HAL_UART_Transmit_IT(&BASE_UART , &BuffTx_content[ BuffTx_bussy ] , 1);

			// Powrót na początek bufora
			if (++BuffTx_bussy >= BUFF_TX_SIZE) BuffTx_bussy = 0;
		} else transmit = 0;
	}
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart) {
	if (huart == &BASE_UART) {

		// ######     Jeżeli dioda nie miga to znaczy, że wystąpił błąd     ######
		// !!!!!!             Zapalanie diody w przypadku błędu             !!!!!!

		//HAL_GPIO_WritePin(GPIOA , GPIO_PIN_5 , GPIO_PIN_SET);
	}
}
