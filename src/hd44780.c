#include "hd44780.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/** *************************************************************************************************************** **/

void HD44780_Initialize() {
    HD44780_RESET_E;
    HD44780_RESET_RS;
    delay(1);

#ifndef HD44780_8BIT_MODE
    HD44780_SendCmd(0x33);
    HD44780_SendCmd(0x32);
    HD44780_SendCmd(CMD_FUNCTION | FUNCTION_2LINES);
#else
    HD44780_SendCmd(CMD_FUNCTION | FUNCTION_2LINES | FUNCTION_8BIT);
#endif
    HD44780_CLEAR;
    HD44780_SendCmd(CMD_CONTROLL | CONTROLL_ON);
    HD44780_SendCmd(CMD_ENTRY_MODE | CONTROLL_ON | ENTRY_RTL);

    // Tekst powitalny
    HD44780_SendString((uint8_t*) "PROGRAM   STOPER" , 16);
    HD44780_SetCursor(0 , 1);
    HD44780_SendString((uint8_t*) " uzyj terminala" , 15);

    // Ustawianie parametrów
    lock_update = 0;
}

/** *************************************************************************************************************** **/

#ifndef HD44780_8BIT_MODE
void HD44780_SendHalf(uint8_t data) {
    HD44780_SET_E;
    HAL_GPIO_WritePin(LcdPorts[port_d4] , LcdPins[pin_d4] , data & 0x01);
    HAL_GPIO_WritePin(LcdPorts[port_d5] , LcdPins[pin_d5] , data & 0x02);
    HAL_GPIO_WritePin(LcdPorts[port_d6] , LcdPins[pin_d6] , data & 0x04);
    HAL_GPIO_WritePin(LcdPorts[port_d7] , LcdPins[pin_d7] , data & 0x08);

    // Opadające zbocze pinu "E" to informacja, że odebrano dane
    delay(1);
    HD44780_RESET_E;
}
#endif

/** *************************************************************************************************************** **/

void HD44780_Send(uint8_t data) {
#ifdef HD44780_8BIT_MODE
    HD44780_SET_E;
    HAL_GPIO_WritePin(LcdPorts[port_d0] , LcdPins[pin_d0] , data & 0x01);
    HAL_GPIO_WritePin(LcdPorts[port_d1] , LcdPins[pin_d1] , data & 0x02);
    HAL_GPIO_WritePin(LcdPorts[port_d2] , LcdPins[pin_d2] , data & 0x04);
    HAL_GPIO_WritePin(LcdPorts[port_d3] , LcdPins[pin_d3] , data & 0x08);
    HAL_GPIO_WritePin(LcdPorts[port_d4] , LcdPins[pin_d4] , data & 0x10);
    HAL_GPIO_WritePin(LcdPorts[port_d5] , LcdPins[pin_d5] , data & 0x20);
    HAL_GPIO_WritePin(LcdPorts[port_d6] , LcdPins[pin_d6] , data & 0x40);
    HAL_GPIO_WritePin(LcdPorts[port_d7] , LcdPins[pin_d7] , data & 0x80);

    delay(1);

    // Opadające zbocze pinu "E" to informacja, że odebrano dane
    HD44780_RESET_E;
#else
    HD44780_SendHalf(data >> 4);    // Starsze bity
    HD44780_SendHalf(data);         // Młodsze bity

    delay(1);
#endif
}

/** *************************************************************************************************************** **/

void HD44780_SendCmd(uint8_t cmd) {
    // Niski stan na pinie "RS" to informacja, że wprowadzana będzie komenda
    HD44780_RESET_RS;
    HD44780_Send(cmd);
}

/** *************************************************************************************************************** **/

void HD44780_SetCursor(uint8_t x , uint8_t y) {
    // 0x0F to max pozycja kursora w poziomie
    if (x > 0x0F) x = 0x0F;
    // 0..1 to dopuszczalne wartości
    if (y > 1) y = 1;

    // Zapisywanie pozycji
    cursor_x = x;
    cursor_y = y;

    // Linia nr 1
    if (y == 0) HD44780_SendCmd(SET_DDRAM | (LINE_1 + x));
    // Linia 2
    else HD44780_SendCmd(SET_DDRAM | (LINE_2 + x));
}

/** *************************************************************************************************************** **/

void HD44780_SendChar(uint8_t chr) {
    // Wysoki stan na pinie "RS" to informacja, że wprowadzana będą dane
    HD44780_SET_RS;
    HD44780_Send(chr);
}

/** *************************************************************************************************************** **/

void HD44780_SendString(uint8_t* str , uint8_t len) {
    for (uint8_t i = 0 ; i < len ; i++)
        HD44780_SendChar(str[i]);
}
