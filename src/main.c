/* USER CODE BEGIN Header */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

uint16_t fun_type = 0;
uint8_t fun_param = 0;
uint16_t fun_data = 0;

// Tekst, który zostanie zapisany do pamięci flash
uint8_t saved_times[ (10 * MAX_SAVED_TIMES) + 1 ];
uint8_t changed = 0;

void update_saved_times(uint8_t min , uint8_t sec , uint16_t mili) {

	if (changed == 0) return;

	for (uint16_t i = MAX_SAVED_TIMES - 1 ; i > 0 ; i--) {
		mid_times_values[i][0] = mid_times_values[i - 1][0];
		mid_times_values[i][1] = mid_times_values[i - 1][1];
		mid_times_values[i][2] = mid_times_values[i - 1][2];
	}

	mid_times_values[0][0] = (uint8_t) min;
	mid_times_values[0][1] = (uint8_t) sec;
	mid_times_values[0][2] = mili;
}

volatile uint8_t btn_state = 0;
void on_blue_clicked() {

	if (btn_state == 1) return;
	else btn_state = 1;

	if (Cntr_state == COUNTER_ST_COUNTING) {
		update_saved_times(Cntr_min , Cntr_sec , Cntr_mili);
		ringbuffer_write((uint8_t*) "[=] Zapisano miedzyczas!\r\n");
		changed = 1;
	} else {
		ringbuffer_write((uint8_t*) "[x] Stoper nie jest uruchomiony!\r\n");
	}

	btn_state = 0;
}

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM2_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */

void buzzz_on(uint16_t tim) {
	HAL_GPIO_WritePin(BUZZ_GPIO_Port , BUZZ_Pin , GPIO_PIN_RESET);
	buzz_time = tim;
}

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM2_Init();
  MX_USART2_UART_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */
  buzzz_off();

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // ######   Zapisywanie ustawionych pinów do tablic w bibliotece wyświetlacza   ######
  // ######     Wyświetlacz domyślnie jest w trybie 4 bitowym - i tak zostaje     ######
  LcdPorts[port_e]		= LCD_E_GPIO_Port;
  LcdPins[pin_e]		= LCD_E_Pin;
  LcdPorts[port_rs]		= LCD_RS_GPIO_Port;
  LcdPins[pin_rs]		= LCD_RS_Pin;
  LcdPorts[port_d4]		= LCD_D4_GPIO_Port;
  LcdPins[pin_d4]		= LCD_D4_Pin;
  LcdPorts[port_d5]		= LCD_D5_GPIO_Port;
  LcdPins[pin_d5]		= LCD_D5_Pin;
  LcdPorts[port_d6]		= LCD_D6_GPIO_Port;
  LcdPins[pin_d6]		= LCD_D6_Pin;
  LcdPorts[port_d7]		= LCD_D7_GPIO_Port;
  LcdPins[pin_d7]		= LCD_D7_Pin;

  // ######                 Czytanie międzyczasów z pamięci FLASH                 ######
  // ######                   Przetwarzanie odebranego stringu                    ######
  flash_read(mid_times_string);

  // Zmienne pomocnicze
  uint8_t tmp_str[3];
  uint8_t tmp_min;
  uint8_t tmp_sec;
  uint16_t tmp_mili;

  // Pętla przetwarzająca znaki
  for (uint8_t i = 0 , j = 0 ; j < MAX_SAVED_TIMES ; j++) {
	  tmp_str[0] = mid_times_string[i];
	  tmp_str[1] = mid_times_string[i + 1];
	  tmp_str[2] = 0;
	  i+= 3;

	  tmp_min = (uint8_t) atoi((char*) tmp_str);

	  tmp_str[0] = mid_times_string[i];
	  tmp_str[1] = mid_times_string[i + 1];
	  tmp_str[2] = 0;
	  i+= 3;

	  tmp_sec = (uint8_t) atoi((char*) tmp_str);

	  tmp_str[0] = mid_times_string[i];
	  tmp_str[1] = mid_times_string[i + 1];
	  tmp_str[2] = mid_times_string[i + 2];

	  i += 4;

	  tmp_mili = (uint16_t) atoi((char*) tmp_str);

	  mid_times_values[j][0] = tmp_min;
	  mid_times_values[j][1] = tmp_sec;
	  mid_times_values[j][2] = tmp_mili;
  }

  // ######        Wywołanie funkcji inicjalizujących dołączone biblioteki        ######
  HD44780_Initialize();

  // ######                 Nasłuchiwanie na wprowadzanie tekstu                  ######
  HAL_UART_Receive_IT(&huart2 , &BuffRx_content[ BuffRx_empty ] , 1);

  // ######                              Powitanie                                ######
  BuffTx_free = 0;
  ringbuffer_write((uint8_t*) "Witaj! Uruchomiono program \"stoper\".\r\n");

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /* USER CODE END 2 */
 
 

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  int16_t read_rx;
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	  // ######             Jeżeli odliczanie właśnie się zakończyło              ######
	  if (Cntr_state == COUNTER_ST_ZERO) {
		  counter_update_lcd(0);
		  Cntr_state = COUNTER_ST_NULL;

		  // Jeżeli zapisano międzyczasy to zostaną na koniec wpisane do pamięci FLASH
		  if (changed) {
			  memset(mid_times_string , 0 , sizeof (mid_times_string));
			  for (uint8_t i = 0 ; i < MAX_SAVED_TIMES ; i++) {
				  sprintf((char*) mid_times_string , "%s%02d:%02d:%03d|" ,
						  (char*) mid_times_string ,
						  (int) mid_times_values[i][0] ,
						  (int) mid_times_values[i][1] ,
						  (int) mid_times_values[i][2]);
			  }

			  // Zapis do pamięci po zakończeniu odliczania
			  flash_write(mid_times_string);

			  if (Cntr_beep) {
				  buzzz_on(10);
				  delay(250);
				  buzzz_on(10);
			  }
		  }
	  } else if (Cntr_state == COUNTER_ST_COUNTING) {
		  counter_update_lcd(1);
	  }

	  // ######                Odczytywanie nowego znaku z bufora                 ######
	  read_rx = ringbuffer_read();

	  // ######          Jeżeli nie '0' to znaczy, że coś można odczytać          ######
	  if (read_rx >= 0) {
		  // Przekazywanie odebranego znaku do biblioteki protokołu
		  protocol_process_data((uint8_t) read_rx);
	  }

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL2;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* USART2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART2_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(USART2_IRQn);
}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 999;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 15;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  HAL_TIM_Base_Start_IT(&htim2);

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, LCD_E_Pin|LCD_RS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, BUZZ_Pin|LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LCD_D4_Pin|LCD_D5_Pin|LCD_D6_Pin|LCD_D7_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : BTN_Pin */
  GPIO_InitStruct.Pin = BTN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BTN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LCD_E_Pin LCD_RS_Pin */
  GPIO_InitStruct.Pin = LCD_E_Pin|LCD_RS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : BUZZ_Pin LED_Pin */
  GPIO_InitStruct.Pin = BUZZ_Pin|LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LCD_D4_Pin LCD_D5_Pin LCD_D6_Pin LCD_D7_Pin */
  GPIO_InitStruct.Pin = LCD_D4_Pin|LCD_D5_Pin|LCD_D6_Pin|LCD_D7_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI4_15_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

	//HAL_GPIO_WritePin(LED_GPIO_Port , LED_Pin , GPIO_PIN_SET);
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
