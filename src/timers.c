#include "timers.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/** *************************************************************************************************************** **/

void counter_delay(uint16_t ms) {
	delay_var = ms;

	// Zmienna jest dekrementowana w Callbacku po otrzymaniu przerwania
	while (delay_var);
}

/** *************************************************************************************************************** **/

void counter_update_lcd(uint8_t line) {
	char text[17];

	if (line == 0 || line == 1) {
		HD44780_SetCursor(0 , 0);
		HD44780_SendString((uint8_t*) text , (uint8_t) sprintf(text , "   %02d:%02d:%03d    " ,
				(int) Cntr_min , (int)  Cntr_sec , (int) Cntr_mili));
	}

	if (line == 0 || line == 2) {
		memset(text , 0 , sizeof (text));
		if (Cntr_beep) strcat(text , "BUZZ  ");
		else strcat(text , "____  ");

		if (Cntr_state == COUNTER_ST_COUNTING) strcat(text , "____  ");
		else if (Cntr_state == COUNTER_ST_ZERO) strcat(text , "END_  ");
		else strcat(text , "STOP  ");

		if (Cntr_direction == COUNTER_DIR_UP) strcat(text , "__UP");
		else strcat(text , "DOWN");

		HD44780_SetCursor(0 , 1);
		HD44780_SendString((uint8_t*) text , 16);
	}
}

/** *************************************************************************************************************** **/

void counter_init_program(uint8_t min , uint8_t sec , uint8_t dir , uint8_t beep) {
    // Zatrzymywanie licznika
	Cntr_state		= COUNTER_ST_NULL;

    // Ustawianie pól struktury
    Cntr_direction    = (dir == COUNTER_DIR_UP ? COUNTER_DIR_UP : COUNTER_DIR_DOWN);
    Cntr_min          = min;
    Cntr_sec          = sec;
    Cntr_limit_min	  = 0;
    Cntr_limit_sec	  = 0;
    Cntr_mili         = 0;
    Cntr_beep		  = beep ? 0 : 1;

    // Blokowanie wyświetlacza
    lock_update			= 1;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* tim) {
	if (tim == &BASE_TIM) {
		// Jeżeli licznik jest ustawiony to należy go zmniejszać
		if (delay_var > 0) --delay_var;

		// Sprawdzanie opóźnienia dla buzzera
		if (buzz_time > 0) {
			--buzz_time;
			if (buzz_time <= 0)
				buzzz_off();
		}

		// Liczenie
		if (Cntr_state == COUNTER_ST_COUNTING) {
			if (Cntr_direction == COUNTER_DIR_UP) {
				if (++(Cntr_mili) > 999) {
					Cntr_mili = 0;

					if (++(Cntr_sec) > 59) {
						Cntr_sec = 0;
						++Cntr_min;
					}
				}
			} else {
				if (Cntr_mili > 0) Cntr_mili--;
				else {
					Cntr_mili = 999;

					if (Cntr_sec > 0) Cntr_sec--;
					else {
						Cntr_sec = 59;

						if (Cntr_min > 0) Cntr_min--;
					}

				}
			}

			// Jeżeli doliczono do końca
			if (Cntr_min == Cntr_limit_min && Cntr_sec == Cntr_limit_sec) {
				Cntr_state = COUNTER_ST_ZERO;
			}
		}
	}
}
