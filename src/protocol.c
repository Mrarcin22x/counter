#include "protocol.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void protocol_process_data(uint8_t data) {
	//
	// --- Błędy transmisji -------------------------------------------------------------------------------------------
	// Wykrycie błędu przerywa odbieranie znaku
	//
	// Jeżeli wystąpił błąd transmisji to dane inne niż początek rmki będą odrzucane
	if (proto_state == STATE_ERROR && data != FRAME_START)
		return;
	//
	// Otrzymywanie danych bez znaku startu ramki
	if (!GET_PROGRESS_START() && data != FRAME_START) {

		// Ustawianie stanu błędu
		proto_state = STATE_ERROR;
		return;

	}
	//
	// Jeżeli transmisja powinna się skończyć a nie otrzymano znaku końca
	if (GET_PROGRESS_DATA() && data != FRAME_STOP && data != FRAME_START) {

		// Ustawianie flagi błędu z komunikatem
		protocol_write_error((uint8_t*) "[x] Oczekiwano na znak konca ramki!\r\n");

		return;
	}
	//
	// Znaki sterujące są nieprawidłowe
	if (data < 32) {

		// Ustawianie flagi błędu z komunikatem
		protocol_write_error((uint8_t*) "[x] Protokol nie obsluguje znakow sterujacych!");

		return;

	}
	//
	// --- Start ------------------------------------------------------------------------------------------------------
	// Start rozpoczynany jest specjalnym znakiem FRAME_START
	//
	if (data == FRAME_START) {

		// Komunikat i przerywanie funkcji
		if (GET_PROGRESS_STOP()) ringbuffer_write((uint8_t*) "[<] Start\r\n");
		else ringbuffer_write((uint8_t*) "[<] Ponowny start\r\n");

		// Czyszczenie wszystkich danych
		protocol_clear();

		// Zaznaczono, że otrzymano już znak startu
		SET_PROGRESS_START();

		return;

	}
	//
	// --- Stop -------------------------------------------------------------------------------------------------------
	// Stop określony jest znakiem FRAME_STOP
	//
	if (data == FRAME_STOP) {

		// Kończenie transmisji
		SET_PROGRESS_STOP();

		// Jeżeli komenda została w całości przesłana
		if (GET_PROGRESS_CMD3()) {

			// Jeżeli określono rozmiar danych
			if (GET_PROGRESS_SIZE()) {

				// Rozmiar danych się zgadza się z tym, który został określony
				if (proto_size == 0 || GET_PROGRESS_DATA()) {

					// Ustawianie flagi
					proto_state = STATE_PROCESSED;

					// Komunikat i przerywanie funkcji
					ringbuffer_write((uint8_t*) "[>] Stop.\r\n");
					uint8_t frame[32];
					sprintf((char*) frame , "[ok] CMD: %s SIZ: %i DATA: %s\r\n" , (char*) proto_command , proto_size , (char*) proto_data);
					ringbuffer_write(frame);

					// Przekazywanie komendy do przetworzoenia
					protocol_process_cmd(proto_command , proto_data);

				} else {

					// Ustawianie flagi błędu z komunikatem
					protocol_write_error((uint8_t*) "[x] Otrzymalem znak konca ramki - za szybko!\r\n");
				}

			// Jeżeli nie to znaczy, że za szybko zakończono ramkę
			} else {

				// Ustawianie flagi błędu z komunikatem
				protocol_write_error((uint8_t*) "[x] Otrzymalem znak konca ramki - za szybko!\r\n");

			}

			return;
		}
	}
	//
	// --- Komenda ----------------------------------------------------------------------------------------------------
	// Komenda musi skladac się z litery i 2 cyfr
	//
	if (!GET_PROGRESS_CMD1()) {

		// Jeżeli ustawiono flagę ucieczki
		if (proto_escape) {
			proto_escape = 0;
			if (data == '(') proto_command[0] = '<';
			else if (data == ')') proto_command[0] = '>';
			else if (data == 'S') proto_command[0] = '$';
			else {
				proto_state = STATE_ERROR;
				protocol_write_error((uint8_t*) "[x] Blad znaku ucieczki!\r\n");
				return;
			}

		// Jeżeli podano znak ucieczki
		} else if (data == '$') {
			proto_escape = 1;
			return;
		// Inny znak
		} else {
			proto_command[0] = data;
		}

		SET_PROGRESS_CMD1();
		return;
	}
	//
	if (!GET_PROGRESS_CMD2()) {

		// Jeżeli ustawiono flagę ucieczki
		if (proto_escape) {
			proto_escape = 0;
			if (data == '(') proto_command[1] = '<';
			else if (data == ')') proto_command[1] = '>';
			else if (data == 'S') proto_command[1] = '$';
			else  {
				proto_state = STATE_ERROR;
				protocol_write_error((uint8_t*) "[x] Blad znaku ucieczki!\r\n");
				return;
			}

		// Jeżeli podano znak ucieczki
		} else if (data == '$') {
			proto_escape = 1;
			return;
		// Inny znak
		} else {
			proto_command[1] = data;
		}

		SET_PROGRESS_CMD2();
		return;
	}
	//
	if (!GET_PROGRESS_CMD3()) {

		// Jeżeli ustawiono flagę ucieczki
		if (proto_escape) {
			proto_escape = 0;
			if (data == '(') proto_command[2] = '<';
			else if (data == ')') proto_command[2] = '>';
			else if (data == 'S') proto_command[2] = '$';
			else  {
				proto_state = STATE_ERROR;
				protocol_write_error((uint8_t*) "[x] Blad znaku ucieczki!\r\n");
				return;
			}

		// Jeżeli podano znak ucieczki
		} else if (data == '$') {
			proto_escape = 1;
			return;
		// Inny znak
		} else {
			proto_command[2] = data;
		}

		SET_PROGRESS_CMD3();
		return;
	}
	//
	// --- Rozmiar danych ---------------------------------------------------------------------------------------------
	// Rozmiar usi być cyfrą od 0-8. W przypadku wartości 0 dane nie będą odbierane.
	//
	if (!GET_PROGRESS_SIZE()) {

		// Tylko cyfra może zostać odebrana
		if (data >= '0' && data <= '8') {

			// Zapisywanie znaku do tablicy
			proto_size = DIGIT_TO_NUMBER(data);
			SET_PROGRESS_SIZE();

		} else {

			// Komunikat
			protocol_write_error((uint8_t*) "[x] Okreslono zly rozmiar danych!\r\n");

		}

		return;
	}
	//
	// --- Dane -------------------------------------------------------------------------------------------------------
	// Rozmiar danych określa ile znaków będą zawierały dane. Ułatwia to odebranie następnych pól ramki.
	// Jednak komend anie musi pobierać danych dlatego w przypadku rozmiaru 0 dane nie będą odbierane.
	//
	if (!GET_PROGRESS_DATA() && proto_size != 0) {

		// Jeżeli ustawiono flagę ucieczki
		if (proto_escape) {
			proto_escape = 0;
			if (data == '(') proto_data[ proto_iter ] = '<';
			else if (data == ')') proto_data[ proto_iter ] = '>';
			else if (data == 'S') proto_data[ proto_iter ] = '$';
			else  {
				proto_state = STATE_ERROR;
				protocol_write_error((uint8_t*) "[x] Blad znaku ucieczki!\r\n");
				return;
			}

		// Jeżeli podano znak ucieczki
		} else if (data == '$') {
			proto_escape = 1;
			return;
		// Inny znak
		} else {
			proto_data[ proto_iter ] = data;
		}

		// Ustawianie flagi odebraia danych jeśli trzeba
		if (++proto_iter >= proto_size) {

			SET_PROGRESS_DATA();

		}

		return;
	}
}

/** *************************************************************************************************************** **/

void protocol_process_cmd(uint8_t* cmd , uint8_t* data) {

	char text[128];
	memset(text , 0 , sizeof (text));

	// Sczegóły otrzymanych danych
	//sprintf(text , "\r\n========\r\nCMD: %s\r\nDATA: %s (#%i)\r\n========\r\n" , (char*) cmd , (char*) data , strlen((char*) data));
	//ringbuffer_write((uint8_t*) text);

	// --- [C] STOPER -------------------------------------------------------------------------------------------------

	if (cmd[0] == 'c' || cmd[0] == 'C') {

		// --- Reset --------------------------------------------------------------------------------------------------

		if (cmd[1] == '0' && cmd[2] == '0') {

			Cntr_state = COUNTER_ST_NULL;
			ringbuffer_write((uint8_t*) "[i] OK.\r\n");
			HD44780_CLEAR;
			HD44780_SetCursor(0 , 0);
			HD44780_SendString((uint8_t*) "Zamknieto." , 10);

		// --- Start / Stop -------------------------------------------------------------------------------------------

		} else if (cmd[1] == '0' && cmd[2] == '1') {

			// Jeżeli rozmiar danych to 1 i wartość jest 0..1
			if ((data[0] == '0' || data[0] == '1') && strlen((char*) data) == 1) {

				// STOP
				if (data['0'] == '1') {

					if (Cntr_state != COUNTER_ST_COUNTING) {
						ringbuffer_write((uint8_t*) "[i] Stoper nie jest aktywny!\r\n");
					} else {
						Cntr_state = COUNTER_ST_STOP;
						ringbuffer_write((uint8_t*) "[i] Zatrzymano stoper (zablokowano miedzyczasy)!\r\n");
					}

				// START
				} else {

					if (Cntr_state == COUNTER_ST_ZERO) {
						ringbuffer_write((uint8_t*) "[i] Stoper zakonczyl odliczanie!\r\n");
					} else {
						Cntr_state = COUNTER_ST_COUNTING;
						ringbuffer_write((uint8_t*) "[i] Uruchomiono licznik (odblokowano miedzyczasy)!\r\n");
					}
				}

				// Komunikat
				counter_update_lcd(0);
				ringbuffer_write((uint8_t*) "[i] OK.\r\n");


			// Inny błąd...
			} else {
				ringbuffer_write((uint8_t*) "[x] Podano zly parametr!\r\n");
			}


		// --- Międzyczasy --------------------------------------------------------------------------------------------

		} else if (cmd[1] == '0' && cmd[2] == '2') {

			if (Cntr_state != COUNTER_ST_NULL) {

				ringbuffer_write((uint8_t*) "[x] Nie mozna modyfikowac dzialajacego programu!\r\n");

			} else {

				memset(text , 0 , sizeof (text));
				memcpy(text , data , strlen((char*) data));

				uint8_t num = atoi(text);

				if (num < 1 || num > 5) {
					ringbuffer_write((uint8_t*) "[x] Nieprawidłowy parametr. Podaj liczbę 1..5.\r\n");
				} else {

					ringbuffer_write((uint8_t*) "---------\r\n");

					uint8_t tmp[16];
					for (uint8_t i = 0 ; i < num ; i++) {
						sprintf((char*) tmp , "%d) %02d:%02d:%03d\r\n" ,
							((int) i) + 1 , (int) mid_times_values[i][0] , (int) mid_times_values[i][1] , (int) mid_times_values[i][2]);
						ringbuffer_write(tmp);
					}

					ringbuffer_write((uint8_t*) "---------\r\n");
				}
			}

		// Kierunek liczenia
		} else if (cmd[1] == '0' && cmd[2] == '3') {

			// Jeżeli stan nie jest "NONE" to znaczy, że program działa i nie można go modyfikować
			if (Cntr_state != COUNTER_ST_NULL) {

				ringbuffer_write((uint8_t*) "[x] Nie mozna modyfikowac dzialajacego programu!\r\n");

			// Jeżeli rozmiar danych to 1 i wartość jest 0..1
			} else if ((data[0] == '0' || data[0] == '1') && strlen((char*) data) == 1) {

				// Zmiana ustawień i aktualizacja wyświetlacza
				if (data[0] == '0') {

					if (Cntr_direction == COUNTER_DIR_UP) {

						ringbuffer_write((uint8_t*) "[x] Juz licze w gore!\r\n");
						return;

					} else {

						Cntr_direction		= COUNTER_DIR_UP;
						Cntr_limit_min		= Cntr_min;
						Cntr_limit_sec		= Cntr_sec;
						Cntr_min			= 0;
						Cntr_sec			= 0;
					}

				} else {

					if (Cntr_direction == COUNTER_DIR_DOWN) {

						ringbuffer_write((uint8_t*) "[x] Juz licze w dol!\r\n");
						return;

					} else {

						Cntr_direction		= COUNTER_DIR_DOWN;
						Cntr_min			= Cntr_limit_min;
						Cntr_sec			= Cntr_limit_sec;
						Cntr_limit_min		= 0;
						Cntr_limit_sec		= 0;
					}

				}

				Cntr_mili = 0;
				counter_update_lcd(0);

				// Komunikat
				ringbuffer_write((uint8_t*) "[i] OK.\r\n");


			// Inny błąd...
			} else {
				ringbuffer_write((uint8_t*) "[x] Podano zly parametr!\r\n");
			}

		// Ustawienie czasu
		} else if (cmd[1] == '0' && cmd[2] == '4') {


			// Jeżeli stan nie jest "NONE" to znaczy, że program działa i nie można go modyfikować
			if (Cntr_state != COUNTER_ST_NULL) {

				ringbuffer_write((uint8_t*) "[x] Nie mozna modyfikowac dzialajacego programu!\r\n");

			// Sprawdzanie formatu i rozmiaru
			} else if (strlen((char*) data) == 5 &&
					   IS_NUMBER(data[0]) && IS_NUMBER(data[1]) &&
					   data[2] == ':' &&
					   IS_NUMBER(data[3]) && IS_NUMBER(data[4])) {

				uint8_t min;
				uint8_t sec;

				// wydobywanie minut
				sprintf(text , "%c%c" , data[0] , data[1]);
				min = (uint8_t) atoi(text);

				// wydobywanie sekund
				sprintf(text , "%c%c" , data[3] , data[4]);
				sec = (uint8_t) atoi(text);

				if (min < 100 && sec < 60) {

					if (Cntr_direction == COUNTER_DIR_UP) {
						Cntr_min			= 0;
						Cntr_sec			= 0;
						Cntr_limit_min	 	= min;
						Cntr_limit_sec	 	= sec;
					} else {
						Cntr_limit_min		= min;
						Cntr_limit_sec		= sec;
						Cntr_min	 		= 0;
						Cntr_sec		 	= 0;
					}

					Cntr_mili				= 0;

					counter_update_lcd(0);
					ringbuffer_write((uint8_t*) "[i] OK.\r\n");

				} else {
					ringbuffer_write((uint8_t*) "[x] Nieprawidlowe wartosci liczbowe!\r\n");
				}

			} else {
				ringbuffer_write((uint8_t*) "[x] Zly format czasu!\r\n");
			}

		// Buzzer
		} else if (cmd[1] == '0' && cmd[2] == '5') {

			// Jeżeli stan nie jest "NONE" to znaczy, że program działa i nie można go modyfikować
			if (Cntr_state != COUNTER_ST_NULL) {

				ringbuffer_write((uint8_t*) "[x] Nie mozna modyfikowac dzialajacego programu!\r\n");

			// Jeżeli rozmiar danych to 1 i wartość jest 0..1
			} else if ((data[0] == '0' || data[0] == '1') && strlen((char*) data) == 1) {

				// Zmiana ustawień i aktualizacja wyświetlacza
				Cntr_beep = data[0] == '0' ? 0 : 1;
				counter_update_lcd(0);
				buzzz_on(5);

				// Komunikat
				ringbuffer_write((uint8_t*) "[i] OK.\r\n");


			// Inny błąd...
			} else {
				ringbuffer_write((uint8_t*) "[x] Podano zly parametr!\r\n");
			}

		} else {
			ringbuffer_write((uint8_t*) "[x] Nieobslugiwana komenda licznika!\r\n");
		}

		return;

	// --- [L] KOMENDY DLA LCD ----------------------------------------------------------------------------------------

	} else if (cmd[0] == 'l' || cmd[0] == 'L') {

		// Jeżeli działa program to obsługa wyświetlacza nie powinna być możliwa
		if (lock_update)
			ringbuffer_write((uint8_t*) "[x] Wyswietlacz zablokowany przez program!\r\n");

		// Clear
		else if (cmd[1] == '0' && cmd[2] == '0') {
			HD44780_CLEAR;
			ringbuffer_write((uint8_t*) "[i] Wyczyszczono wyswietlacz!\r\n");

		// Poz X
		} else if (cmd[1] == '0' && cmd[2] == '1') {
			memset(text , 0 , sizeof (text));
			memcpy(text , data , strlen((char*) data));

			uint8_t x = atoi(text);

			if (x < 0 || x > 15) {
				ringbuffer_write((uint8_t*) "[x] Nieprawidłowy parametr. Podaj liczbę 0..15.\r\n");
			} else {
				HD44780_SetCursor(x , cursor_y);
				ringbuffer_write((uint8_t*) "[i] OK.\r\n");
			}

		// Poz Y
		} else if (cmd[1] == '0' && cmd[2] == '2') {
			memset(text , 0 , sizeof (text));
			text[0] = data[0];
			uint8_t y = atoi(text);

			if (y < 0 || y > 1) {
				ringbuffer_write((uint8_t*) "[x] Nieprawidłowy parametr. Podaj liczbę 0..1.\r\n");
			} else {
				HD44780_SetCursor(cursor_x , y);
				ringbuffer_write((uint8_t*) "[i] OK.\r\n");
			}

		// Pisanie na wyświetlaczu
		} else if (cmd[1] == '0' && cmd[2] == '3') {
			memset(text , 0 , sizeof (text));
			memcpy(text , data , strlen((char*) data));

			HD44780_SendString((uint8_t*) text , (uint8_t) strlen(text));
			ringbuffer_write((uint8_t*) "[i] OK.\r\n");
		} else {
			ringbuffer_write((uint8_t*) "[*] Nieznana komenda wyswietlacza!\r\n");
		}

		return;
	} else {
		ringbuffer_write((uint8_t*) "[*] Nieznana komenda!\r\n");
	}

}

/** *************************************************************************************************************** **/

void protocol_write_error(uint8_t* err) {

	// Ustawienie flagi błędu
	proto_state = STATE_ERROR;

	// Treść problemu + kod błędu
	if (strlen((char*) err) != 0) {
		char text[128];
		sprintf(text , "%s[#] Blad: 0x%02X\r\n\r\n" , (char*) err , (int) proto_progress);
		ringbuffer_write((uint8_t*) text);
	}
}


/** *************************************************************************************************************** **/

void protocol_clear() {
	proto_progress		= 0;
	proto_size			= SIZE_UNSET;
	proto_escape		= 0;
	proto_iter			= 0;
	proto_state			= STATE_NONE;
	memset(proto_data , 0 , sizeof (proto_data));
	memset(proto_command , 0 , sizeof (proto_command));
}
